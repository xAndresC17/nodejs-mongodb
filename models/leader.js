const mongose = require('mongoose');

const Schema = mongose.Schema;

const leaderSchema = new Schema({
    name: {
        type:String,
        requiered: true,
        unique: true
    },
    image:{
        type: String,
        requiered:true
    },
    designation:{
        type:String,
        requiered:true
    },
    abbr:{
        type:String,
        requiered:true
    },
    featured:{
        type:Boolean,
        default:false
    },
    description:{
        type:String,
        requiered:true
    }

},{
    timestamps: true
})

var leader = mongose.model('leader',leaderSchema);

module.exports = leader;