const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const favoriteSchema = new Schema({
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require:true
    },
    dishes:[ {type:mongoose.Schema.Types.ObjectId, ref:'Dish',require:true}]
}, {
    timestamps: true
});

const favorite = mongoose.model('favorites',favoriteSchema);

module.exports= favorite;