const express = require('express');
const bodyParser = require('body-Parser');
const Favorite = require('../models/favorite');
const authentication = require('../authenticate');

const favoriteRoutes = express.Router();
favoriteRoutes.use(bodyParser.json());

favoriteRoutes.route('/')
    .get(authentication.verifyUser, (req, res, next) => {

        Favorite.findOne({ 'user': req.user })
            .populate('user')
            .populate('dishes')
            .then((favorites) => {
                res.setHeader('Content-Type', 'application/json');
                res.json({ favorites });
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .post(authentication.verifyUser, (req, res, next) => {

        Favorite.findOne({ 'user': req.user })
            .then((favorite) => {
                if (favorite != null) {
                    let sw = false;

                    favorite.dishes.forEach(element => {
                        if (element == req.body._id) {
                            sw = true;
                        }
                    });

                    if (sw) {
                        res.setHeader('Content-Type', 'application/json');
                        res.json({ 'favorite': false, 'err': 'EXIST', 'data': favorite });
                    } else {
                        favorite.dishes.push(req.body);
                        favorite.save().then((favoriteOld) => {
                            res.setHeader('Content-Type', 'application/json');
                            res.json({ 'favorite': true, 'text': 'ADD', 'data': favoriteOld });
                        })
                    }
                } else {
                    Favorite.create({ 'user': req.user })
                        .then((favorite) => {
                            favorite.dishes.push(req.body);
                            favorite.save().
                                then((favoriteNew) => {
                                    res.setHeader('Content-Type', 'application/json');
                                    res.json({ 'favorite': true, 'text': 'New', 'data': favoriteNew });
                                })

                        })

                }
            }, (err) => next(err))
            .catch((err) => next(err))

    })
    .put(authentication.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /favorites/');
    })
    .delete(authentication.verifyUser, authentication.verifyAdmin, (req, res, next) => {
        Favorite.findOneAndRemove({ 'user': req.user })
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    })

favoriteRoutes.route('/:favorite')
    .get(authentication.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('GET operation not supported on /favorites/');
    })
    .post(authentication.verifyUser, (req, res, next) => {

        Favorite.findOne({ 'user': req.user })
            .then((favorite) => {
                if (favorite != null) {
                    let sw = false;

                    favorite.dishes.forEach(element => {
                        if (element == req.params.favorite) {
                            sw = true;
                        }
                    });

                    if (sw) {
                        res.setHeader('Content-Type', 'application/json');
                        res.json({ 'favorite': false, 'err': 'EXIST', 'data': favorite });
                    } else {
                        favorite.dishes.push(req.params.favorite);
                        favorite.save().then((favoriteOld) => {
                            res.setHeader('Content-Type', 'application/json');
                            res.json({ 'favorite': true, 'text': 'ADD', 'data': favoriteOld });
                        })
                    }
                } else {
                    Favorite.create({ 'user': req.user })
                        .then((favorite) => {
                            favorite.dishes.push(req.params.favorite);
                            favorite.save().
                                then((favoriteNew) => {
                                    res.setHeader('Content-Type', 'application/json');
                                    res.json({ 'favorite': true, 'text': 'New', 'data': favoriteNew });
                                })

                        })

                }
            }, (err) => next(err))
            .catch((err) => next(err))


    })
    .put(authentication.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /favorites/');
    })
    .delete(authentication.verifyUser, (req, res, next) => {
        Favorite.findOne({ 'user': req.user })
            .populate('user')
            .then((favoritesUser) => {

                let index = favoritesUser.dishes.indexOf(req.params.favorite);
                console.log(index);
                if(index>=0){
                    favoritesUser.dishes.splice(index,1);
                    favoritesUser.save()
                        .then((dataDeleted) => {
                            res.setHeader('Content-Type', 'application/json');
                            res.json({ 'favorite': true, 'text': 'Modificated', 'data': dataDeleted });
                        })
                }else{
                    res.setHeader('Content-Type', 'application/json');
                            res.json({ 'favorite': false, 'text': 'Not Found', 'data': favoritesUser });
                }
              

            }, (err) => next(err))
            .catch((err) => next(err)); 1
    })

module.exports = favoriteRoutes;