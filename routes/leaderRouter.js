const express = require('express');
const bodyParser = require('body-parser');
const leader = require('../models/leader');
const authenticate = require('../authenticate');

const leadersRouter = express.Router();
leadersRouter.use(bodyParser.json());

leadersRouter.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain')
        next();
    })
    .get((req, res, next) => {
        leader.find()
            .then((result) => {
                res.setHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                res.json(result);
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .post(authenticate.verifyUser, (req, res, next) => {
        leader.create(req.body)
            .then((result) => {
                res.setHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                res.json(result);
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .put(authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end(`PUT operation not supported on /leaders`)
    })
    .delete(authenticate.verifyUser, (req, res, next) => {
        leader.remove({})
        .then((resp) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(resp);
        }, (err) => next(err))
        .catch((err) => next(err));    
    })

leadersRouter.route('/:leaderId')
    .get((req, res, next) => {

        leader.findById(req.params.leaderId)
            .then((result) => {
                res.setHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                res.json(result)
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .post(authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end(`POST operation not supported on /leaders/${req.params.leaderId}`)
    })
    .put(authenticate.verifyUser, (req, res, next) => {
       
        leader.findByIdAndUpdate(req.params.leaderId,{
            $set:req.body
        }, {new: true})
        .then((result)=>{
            res.setHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                res.json(result)
        },(err)=>next(err))
        .catch((err)=>next(err))
    })
    .delete(authenticate.verifyUser, (req, res, next) => {
        leader.findByIdAndDelete(req.params.leaderId)
        .then((result)=>{
            res.setHeader('Content-Type', 'application/json');
            res.statusCode = 200;
            res.json(result)
        },(err)=>next(err))
        .catch((err)=>next(err))
    })

module.exports = leadersRouter;