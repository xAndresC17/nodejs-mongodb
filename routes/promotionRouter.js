const express = require('express');
const bodyParser = require('body-parser');
const promotion = require('../models/promotion');
const authenticate = require('../authenticate');

const promotionsRouter = express.Router();
promotionsRouter.use(bodyParser.json());

promotionsRouter.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();s
    })
    .get((req, res, next) => {
        promotion.find()
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((errr) => next(err));
    })
    .post(authenticate.verifyUser, (req, res, next) => {
        promotion.create(req.body)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .put(authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end(`PUT operation not supported on /operations`)
    })
    .delete(authenticate.verifyUser, (req, res, next) => {
        promotion.remove({})
        .then((resp) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(resp);
        }, (err) => next(err))
        .catch((err) => next(err));    
    })


promotionsRouter.route('/:promoId')
    .get((req, res, next) => {

        promotion.findById(req.params.promoId)
            .then((resp) => {
                res.setHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .post(authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation not supported on /promotions/'+ req.params.promoId);
       
    })
    .put(authenticate.verifyUser, (req, res, next) => {
        promotion.findByIdAndUpdate(req.params.promoId, {
            $set: req.body
        }, {new: true})
        .then((resp) => {
            res.setHeader('Content-Type', 'application/json');
            res.statusCode = 200;
            res.json(resp);
        }, (err) => next(err))
        .catch((err) => next(err))
    })
    .delete(authenticate.verifyUser, (req, res, next) => {
       
        promotion.findByIdAndDelete(req.params.promoId)
        .then((resp) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(resp);
        }, (err) => next(err))
        .catch((err) => next(err));
    })

module.exports = promotionsRouter;